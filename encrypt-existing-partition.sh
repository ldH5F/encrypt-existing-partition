#!/bin/bash
better-fs(){
	mkdir test
	mount $device test
	btrfs filesystem resize -32M test
	umount test
	cryptsetup reencrypt --encrypt --reduce-device-size 32M $device
	exit
}
exteefour() {
	e2fsck -f $device
	resize2fs -p -M $device
	cryptsetup reencrypt --encrypt --reduce-device-size 32M $device
	cryptsetup open $device recrypt
	resize2fs /dev/mapper/recrypt
	cryptsetup close recrypt
	exit
}
which btrfs || clear ; echo "please install btrfs-progs" ; exit
which cryptsetup || clear ; echo "please install cryptsetup" ; exit
which e2fsck || clear ; echo "please install e2fsck" ; exit
which resize2fs || clear ; echo "please install resize2fs" ; exit
clear
lsblk 
echo "please set your device" 
read device
umount $device
clear 
fs=$(lsblk -fd $device -o FSTYPE | sed "1d")
[ "$fs" == "btrfs" ] && better-fs
[ "$fs" == "ext4" ] && exteefour
clear ; echo "this filesystem is not supported" ; exit
